package api.testingItemGen;

/**
 * Created by JPaul on 3/11/2016.
 */
public enum  Rarity
{
    LEGENDARY,
    RARE,
    UNCOMMON,
    COMMON;
}
