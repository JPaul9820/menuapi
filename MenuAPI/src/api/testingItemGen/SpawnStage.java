package api.testingItemGen;

/**
 * Created by JPaul on 3/10/2016.
 */
public enum SpawnStage
{
    ITEMTYPE_CHOICE,
    ARMOUR_CHOICE,
    WEAPON_CHOICE,
    TIER_CHOICE,
    RARITY_CHOICE;
}
