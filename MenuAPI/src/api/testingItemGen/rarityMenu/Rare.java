package api.testingItemGen.rarityMenu;

import api.Manager;
import api.MenuItem;
import api.SelectorType;
import api.testingItemGen.ParentItemMenu;
import api.testingItemGen.Rarity;
import api.testingItemGen.Tier;
import api.testingItemGen.selectorMenu.SelectorMenu;
import api.util.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;

/**
 * Created by JPaul on 3/8/2016.
 */
public class Rare implements MenuItem
{
    Tier tier;

    @Override
    public ItemStack getItem()
    {
        return new ItemBuilder(Material.DIAMOND)
                .name(ChatColor.RED + "Rare")
                .lore(ChatColor.GRAY + "Generate a random rare item")
                .item();
    }

    @Override
    public void onExecute(Player player, ClickType clickType)
    {
        ParentItemMenu parentItemMenu = (ParentItemMenu) Manager.getMenuManager().getMenu(player).getParent();

        ItemStack item = null;

        if (parentItemMenu.getSelectorType() == SelectorType.WEAPON)
        {
            //item = generate item here
        }
        else if (parentItemMenu.getSelectorType() == SelectorType.ARMOUR)
        {
            //item = generate item here
        }

        player.getInventory().addItem(item);

        Manager.getMenuManager().getMenu(SelectorMenu.class).showToPlayer(player);
    }
}
