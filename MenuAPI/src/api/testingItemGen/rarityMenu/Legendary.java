package api.testingItemGen.rarityMenu;

import api.Manager;
import api.MenuItem;
import api.SelectorType;
import api.testingItemGen.ParentItemMenu;
import api.testingItemGen.Rarity;
import api.testingItemGen.selectorMenu.SelectorMenu;
import api.util.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;

/**
 * Created by JPaul on 3/8/2016.
 */
public class Legendary implements MenuItem
{
    @Override
    public ItemStack getItem()
    {
        return new ItemBuilder(Material.GOLD_INGOT)
                .name(ChatColor.RED + "Legendary")
                .lore(ChatColor.GRAY + "Generate a random legendary item")
                .item();
    }

    @Override
    public void onExecute(Player player, ClickType clickType)
    {
        ParentItemMenu parentItemMenu = (ParentItemMenu) Manager.getMenuManager().getMenu(player).getParent();

        ItemStack item = null;

        if (parentItemMenu.getSelectorType() == SelectorType.WEAPON)
        {
            //item = generate item here
        }
        else if (parentItemMenu.getSelectorType() == SelectorType.ARMOUR)
        {
            //item = generate item here
        }

        player.getInventory().addItem(item);
        Manager.getMenuManager().getMenu(SelectorMenu.class).showToPlayer(player);
    }
}
