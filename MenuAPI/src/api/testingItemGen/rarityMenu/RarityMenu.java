package api.testingItemGen.rarityMenu;

import api.types.Menu;

/**
 * Created by JPaul on 3/8/2016.
 */
public class RarityMenu extends Menu
{
    public RarityMenu()
    {
        super(9, "Rarity Test");

        addItem(0, new Common());
        addItem(1, new Uncommon());
        addItem(2, new Rare());
        addItem(3, new Legendary());
    }
}
