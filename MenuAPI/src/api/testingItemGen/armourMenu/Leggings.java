package api.testingItemGen.armourMenu;

import api.Container;
import api.Manager;
import api.MenuItem;
import api.testingItemGen.Tier;
import api.testingItemGen.rarityMenu.RarityMenu;
import api.util.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

/**
 * Created by JPaul on 3/8/2016.
 */
public class Leggings implements MenuItem, Container<Tier>
{
    Tier tier;

    @Override
    public void setData(Tier object)
    {
        this.tier = object;
    }

    @Override
    public ItemStack getItem()
    {
        return new ItemBuilder(Material.LEATHER_LEGGINGS)
                .name("TEST LEGGINGS")
                .lore("TEST LORE")
                .leatherColour(tier.getColor())
                .item();
    }

    @Override
    public void onExecute(Player player, ClickType clickType)
    {
        Manager.getMenuManager().getMenu(RarityMenu.class).showToPlayer(player);
    }
}
