package api.testingItemGen.armourMenu;

import api.testingItemGen.ParentItemMenu;
import api.testingItemGen.Tier;
import api.types.Menu;
import org.bukkit.ChatColor;

/**
 * Created by JPaul on 3/10/2016.
 */
public class ArmourMenu extends Menu
{

    public ArmourMenu()
    {
        super(9, ChatColor.YELLOW + "Armour Selector");
    }

    public void addItems()
    {
        Tier tier = ((ParentItemMenu) getParent()).getTier();

        addItem(0, new Boots(), tier);
        addItem(1, new Leggings(), tier);
        addItem(2, new Chestplate(), tier);
        addItem(3, new Helmet(), tier);
    }
}
