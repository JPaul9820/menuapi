package api.testingItemGen.tierChoice;

import api.types.Menu;
import org.bukkit.ChatColor;

/**
 * Created by JPaul on 3/8/2016.
 */
public class TierMenu extends Menu
{
    public TierMenu()
    {
        super(9, ChatColor.YELLOW + "Tier Selector");

        addItem(0, new Tier1Purple());
        addItem(1, new Tier2Silver());
        addItem(2, new Tier3Orange());
        addItem(3, new Tier4Teal());
        addItem(4, new Tier5Yellow());
    }
}
