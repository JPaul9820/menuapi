package api.testingItemGen.tierChoice;

import api.Manager;
import api.MenuItem;
import api.SelectorType;
import api.testingItemGen.ParentItemMenu;
import api.testingItemGen.Tier;
import api.testingItemGen.armourMenu.ArmourMenu;
import api.testingItemGen.weaponChoice.WeaponMenu;
import api.util.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

/**
 * Created by JPaul on 3/10/2016.
 */
public class Tier3Orange implements MenuItem
{
    @Override
    public ItemStack getItem()
    {
        return new ItemBuilder(Material.WOOL)
                .name(ChatColor.GOLD + "Tier 3")
                .data(DyeColor.ORANGE.getData())
                .item();
    }

    @Override
    public void onExecute(Player player, ClickType clickType)
    {
        ParentItemMenu parent = (ParentItemMenu) Manager.getMenuManager().getMenu(player).getParent();
        parent.setTier(Tier.TIER3);

        if (parent.getSelectorType() == SelectorType.ARMOUR)
        {
            ArmourMenu armourMenu = (ArmourMenu) Manager.getMenuManager().getMenu(ArmourMenu.class);
            armourMenu.addItems();

            armourMenu.showToPlayer(player);
        }
        else if (parent.getSelectorType() == SelectorType.WEAPON)
        {
            WeaponMenu weaponMenu = (WeaponMenu) Manager.getMenuManager().getMenu(WeaponMenu.class);
            weaponMenu.addItems();

            weaponMenu.showToPlayer(player);
        }
    }
}
