package api.testingItemGen.selectorMenu;

import api.Container;
import api.Manager;
import api.MenuItem;
import api.SelectorType;
import api.testingItemGen.ParentItemMenu;
import api.testingItemGen.tierChoice.TierMenu;
import api.util.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

/**
 * Created by JPaul on 3/10/2016.
 */
public class Weapon implements MenuItem, Container<SelectorType>
{
    SelectorType selectorType;

    @Override
    public void setData(SelectorType selectorType)
    {
        this.selectorType = selectorType;
    }

    @Override
    public ItemStack getItem()
    {
        return new ItemBuilder(Material.GOLD_SWORD)
                .name("Weapon")
                .lore(ChatColor.GRAY + "Generate a random weapon with conditions")
                .item();
    }

    @Override
    public void onExecute(Player player, ClickType clickType)
    {
        ParentItemMenu parent = (ParentItemMenu) Manager.getMenuManager().getMenu(player).getParent();

        parent.setSelectorType(SelectorType.WEAPON);

        Manager.getMenuManager().getMenu(TierMenu.class).showToPlayer(player);
    }
}
