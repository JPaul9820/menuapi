package api.testingItemGen.selectorMenu;

import api.SelectorType;
import api.types.Menu;
import org.bukkit.ChatColor;

/**
 * Created by JPaul on 3/10/2016.
 */
public class SelectorMenu extends Menu
{
    public SelectorMenu()
    {
        super(9, ChatColor.YELLOW + "Select your item type");

        addItem(0, new Weapon(), SelectorType.WEAPON);
        addItem(1, new Armour(), SelectorType.ARMOUR);
    }
}
