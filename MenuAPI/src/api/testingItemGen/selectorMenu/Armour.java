package api.testingItemGen.selectorMenu;

import api.Container;
import api.Manager;
import api.MenuItem;
import api.SelectorType;
import api.testingItemGen.ParentItemMenu;
import api.testingItemGen.Tier;
import api.testingItemGen.tierChoice.TierMenu;
import api.util.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

/**
 * Created by JPaul on 3/11/2016.
 */
public class Armour implements MenuItem, Container<Tier>
{
    Tier tier;

    @Override
    public void setData(Tier tier)
    {
        this.tier = tier;
    }

    @Override
    public ItemStack getItem()
    {
        return new ItemBuilder(Material.GOLD_CHESTPLATE)
                .name(ChatColor.RED + "Armour")
                .lore(ChatColor.GRAY + "Generate a random armour piece with conditions")
                .item();
    }

    @Override
    public void onExecute(Player player, ClickType clickType)
    {
        ParentItemMenu parent = (ParentItemMenu) Manager.getMenuManager().getMenu(player).getParent();

        parent.setSelectorType(SelectorType.ARMOUR);

        Manager.getMenuManager().getMenu(TierMenu.class).showToPlayer(player);
    }
}
