package api.testingItemGen;

import org.bukkit.Material;

import javax.annotation.Nullable;

/**
 * Created by JPaul on 3/11/2016.
 */
public enum ItemType
{
    STAFF,
    AXE,
    SWORD,
    BOW,
    SHIELD,

    HELMET,
    CHESTPLATE,
    LEGGINGS,
    BOOTS;


    public static Material getMatFromType(ItemType itemType, @Nullable Tier tier)
    {
        if (tier != null)
        {
            switch (itemType)
            {
                case STAFF:
                    switch (tier)
                    {
                        case TIER1:
                            return Material.WOOD_HOE;
                        case TIER2:
                            return Material.STONE_HOE;
                        case TIER3:
                            return Material.IRON_HOE;
                        case TIER4:
                            return Material.DIAMOND_HOE;
                        case TIER5:
                            return Material.GOLD_HOE;
                    }
                    break;

                case SWORD:
                    switch (tier)
                    {
                        case TIER1:
                            return Material.WOOD_SWORD;
                        case TIER2:
                            return Material.STONE_SWORD;
                        case TIER3:
                            return Material.IRON_SWORD;
                        case TIER4:
                            return Material.DIAMOND_SWORD;
                        case TIER5:
                            return Material.GOLD_SWORD;
                    }
                    break;

                case AXE:
                    switch (tier)
                    {
                        case TIER1:
                            return Material.WOOD_AXE;
                        case TIER2:
                            return Material.STONE_AXE;
                        case TIER3:
                            return Material.IRON_AXE;
                        case TIER4:
                            return Material.DIAMOND_AXE;
                        case TIER5:
                            return Material.GOLD_AXE;
                    }
                    break;

                case BOW:
                    return Material.BOW;
            }
        }

        switch (itemType)
        {
            case HELMET:
                return Material.LEATHER_HELMET;
            case CHESTPLATE:
                return Material.LEATHER_CHESTPLATE;
            case LEGGINGS:
                return Material.LEATHER_LEGGINGS;
            case BOOTS:
                return Material.LEATHER_BOOTS;
            case SHIELD:
                return Material.SHIELD;
        }

        return null;
    }
}
