package api.testingItemGen;


import api.SelectorType;
import api.types.Menu;
import api.types.ProgressMenu;

import java.util.List;

/**
 * Created by JPaul on 3/10/2016.
 */
public class ParentItemMenu extends ProgressMenu
{
    SelectorType selectorType;
    ItemType itemType;
    Tier tier;

    public ParentItemMenu(int size, String name, List<Menu> menus)
    {
        super(size, name, menus);
    }

    public SelectorType getSelectorType()
    {
        return selectorType;
    }

    public void setSelectorType(SelectorType selectorType)
    {
        this.selectorType = selectorType;
    }

    public ItemType getItemType()
    {
        return itemType;
    }

    public void setItemType(ItemType itemType)
    {
        this.itemType = itemType;
    }

    public Tier getTier()
    {
        return tier;
    }

    public void setTier(Tier tier)
    {
        this.tier = tier;
    }
}
