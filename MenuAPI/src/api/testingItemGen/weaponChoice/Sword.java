package api.testingItemGen.weaponChoice;

import api.Container;
import api.Manager;
import api.MenuItem;
import api.testingItemGen.ItemType;
import api.testingItemGen.ParentItemMenu;
import api.testingItemGen.Tier;
import api.testingItemGen.rarityMenu.RarityMenu;
import api.util.ItemBuilder;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

/**
 * Created by JPaul on 3/10/2016.
 */
public class Sword implements MenuItem, Container<Tier>
{
    Tier tier;

    @Override
    public void setData(Tier tier)
    {
        this.tier = tier;
    }

    @Override
    public ItemStack getItem()
    {
        return new ItemBuilder(ItemType.getMatFromType(ItemType.SWORD, tier))
                .name(tier.getChatColor() + tier.name() + " sword")
                .lore(ChatColor.GRAY + "Generate a random " + tier.name() + " sword")
                .item();
    }

    @Override
    public void onExecute(Player player, ClickType clickType)
    {
        ParentItemMenu parentItemMenu = (ParentItemMenu) Manager.getMenuManager().getMenu(player).getParent();
        parentItemMenu.setItemType(ItemType.SWORD);

        Manager.getMenuManager().getMenu(RarityMenu.class).showToPlayer(player);
    }
}
