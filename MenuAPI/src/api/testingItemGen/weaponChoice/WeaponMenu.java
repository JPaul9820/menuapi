package api.testingItemGen.weaponChoice;

import api.testingItemGen.ParentItemMenu;
import api.testingItemGen.Tier;
import api.types.Menu;
import org.bukkit.ChatColor;

/**
 * Created by JPaul on 3/10/2016.
 */
public class WeaponMenu extends Menu
{
    public WeaponMenu()
    {
        super(9, ChatColor.YELLOW + "Weapon Selector");
    }

    public void addItems()
    {
        Tier tier = ((ParentItemMenu) getParent()).getTier();

        addItem(0, new Axe(), tier);
        addItem(1, new Sword(), tier);
        addItem(2, new Staff(), tier);
        addItem(3, new Bow(), tier);
    }
}
