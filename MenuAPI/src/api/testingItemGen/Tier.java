package api.testingItemGen;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

/**
 * Created by JPaul on 1/18/2016.
 */
public enum Tier
{
    TIER1(Material.WOOD_SWORD, Material.WOOD_AXE, Material.WOOD_HOE, Color.PURPLE, ChatColor.DARK_PURPLE, 1),
    TIER2(Material.STONE_SWORD, Material.STONE_AXE, Material.STONE_HOE, Color.SILVER, ChatColor.GRAY, 2),
    TIER3(Material.IRON_SWORD, Material.IRON_AXE, Material.IRON_HOE, Color.ORANGE, ChatColor.GOLD, 3),
    TIER4(Material.DIAMOND_SWORD, Material.DIAMOND_AXE, Material.DIAMOND_HOE, Color.TEAL, ChatColor.DARK_AQUA, 4),
    TIER5(Material.GOLD_SWORD, Material.GOLD_AXE, Material.GOLD_HOE, Color.fromRGB(255, 223, 0), ChatColor.YELLOW, 5);

    private Material[] materials;
    private Color color;
    private ChatColor chatColor;
    private int tierNumber;

    Tier(Material sword, Material woodAxe, Material woodSword, Color armourColour, ChatColor chatColor, int tierNumber)
    {
        this.materials = new Material[]{sword, woodAxe, woodSword};
        this.color = armourColour;
        this.chatColor = chatColor;
        this.tierNumber = tierNumber;
    }

    public Material[] getMaterials()
    {
        return materials;
    }

    public Color getColor()
    {
        return color;
    }

    public ChatColor getChatColor()
    {
        return chatColor;
    }

    public int getTierNumber()
    {
        return tierNumber;
    }

    public static Tier getTierFromWeapon(Material weapon) { // TODO: make this check if item is armor or weapon, am too lazy atm
        String name = weapon.toString().toLowerCase();

        if (name.startsWith("wood")) {
            return TIER1;
        }
        else if (name.startsWith("stone")) {
            return TIER2;
        }
        else if (name.startsWith("iron")) {
            return TIER3;
        }
        else if (name.startsWith("diamond")) {
            return TIER4;
        }
        else if (name.startsWith("gold"))
        {
            return TIER5;
        }

        return null;
    }

    /*
    public static Tier getTierFromItem(ItemStack item)
    {
        String materialName = item.getType().name().toLowerCase();

        if (ArmourUtil.isLeatherArmour(item))
        {
            LeatherArmorMeta meta = (LeatherArmorMeta) item.getItemMeta();

            if (meta.getColor() == Color.PURPLE)
            {
                return TIER1;
            }
            else if (meta.getColor() == Color.SILVER)
            {
                return TIER2;
            }
            else if (meta.getColor() == Color.ORANGE)
            {
                return TIER3;
            }
            else if (meta.getColor() == Color.TEAL)
            {
                return TIER4;
            }
            else if (meta.getColor() == Color.fromRGB(255, 223, 0))
            {
                return TIER5;
            }
        }
        else
        {
            if (materialName.startsWith("wood"))
            {
                return TIER1;
            }
            else if (materialName.startsWith("stone"))
            {
                return TIER2;
            }
            else if (materialName.startsWith("iron"))
            {
                return TIER3;
            }
            else if (materialName.startsWith("diamond"))
            {
                return TIER4;
            }
            else if (materialName.startsWith("gold"))
            {
                return TIER5;
            }
            else if (materialName.startsWith("bow"))
            {
                String itemName = item.getItemMeta().getDisplayName();

                if (itemName.contains(ChatColor.GREEN.toString()))
                {
                    return TIER1;
                }
                else if (itemName.contains(ChatColor.GRAY.toString()))
                {
                    return TIER2;
                }
                else if (itemName.contains(ChatColor.GOLD.toString()))
                {
                    return TIER3;
                }
                else if (itemName.contains(ChatColor.AQUA.toString()))
                {
                    return TIER4;
                }
                else if (itemName.contains(ChatColor.YELLOW.toString()))
                {
                    return TIER5;
                }
            }
        }

        return null;
    }
    */

    public static Tier getTierFromInt(int tier)
    {
        switch (tier)
        {
            case 1:
                return TIER1;
            case 2:
                return TIER2;
            case 3:
                return TIER3;
            case 4:
                return TIER4;
            case 5:
                return TIER5;
        }

        return null;
    }

    /*
    public static Tier getTierAlias(String string, CommandSender sender)
    {
        switch (string.toLowerCase())
        {
            case "1":
            case "tier1":
                return Tier.TIER1;

            case "2":
            case "tier2":
                return Tier.TIER2;

            case "3":
            case "tier3":
                return Tier.TIER3;

            case "4":
            case "tier4":
                return Tier.TIER4;

            case "5":
            case "tier5":
                return Tier.TIER5;

            default:
                sender.sendMessage(Strings.handleError("That tier alias does not exist, sorry", "tier", "not exist"));
        }

        return null;
    }
    */
}

