package api;

/**
 * Created by JPaul on 3/10/2016.
 */
public enum SelectorType
{
    WEAPON,
    ARMOUR;
}
