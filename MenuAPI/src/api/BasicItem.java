package api;

import api.util.ItemBuilder;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

/**
 * Created by JPaul on 3/9/2016.
 */
public class BasicItem implements MenuItem
{
    ItemStack item;
    OnExecute onExecute;

    public BasicItem(ItemStack item, OnExecute onExecute)
    {
        this.item = item;
        this.onExecute = onExecute;
    }

    public static BasicItem create(ItemStack item, OnExecute execute)
    {
        return new BasicItem(new ItemBuilder(item).item(), execute);
    }

    public static BasicItem create(String name, ItemStack item, OnExecute execute)
    {
        return new BasicItem(new ItemBuilder(item).name(name).item(), execute);
    }

    @Override
    public ItemStack getItem()
    {
        return item;
    }

    @Override
    public void onExecute(Player player, ClickType clickType)
    {
        onExecute.execute(player, clickType);
    }
}
