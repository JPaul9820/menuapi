package api;

/**
 * Created by JPaul on 3/8/2016.
 */
public interface Container<T>
{
    void setData(T t);
}
