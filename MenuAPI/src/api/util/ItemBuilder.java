package api.util;

import com.google.common.collect.Lists;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.material.Wool;

import java.util.Collections;
import java.util.List;

public class ItemBuilder
{
    private ItemStack item;

    public ItemBuilder(Material material)
    {
        item = new ItemStack(material);
    }

    public ItemBuilder(ItemStack item)
    {
        this.item = item;
    }

    public ItemBuilder leatherColour(Color color)
    {
        if (item.getItemMeta() instanceof LeatherArmorMeta)
        {
            LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) item.getItemMeta();
            leatherArmorMeta.setColor(color);
            item.setItemMeta(leatherArmorMeta);
            return this;
        }
        else
        {
            return null;
        }
    }

    public ItemBuilder woolColour(DyeColor dyeColor)
    {
        if (item.getType() == Material.WOOL)
        {
            Wool wool = new Wool(dyeColor);
            ItemStack woolItem = wool.toItemStack();
            woolItem.setAmount(1);
            item = woolItem;
        }
        return this;
    }

    public ItemBuilder data(byte data)
    {
        item.setDurability(data);
        return this;
    }

    public ItemBuilder durability(short durability)
    {
        item.setDurability(durability);
        return this;
    }

    public ItemBuilder type(Material material)
    {
        item.setType(material);
        return this;
    }

    public ItemBuilder amount(int amount)
    {
        item.setAmount(amount);
        return this;
    }

    public ItemBuilder name(String name)
    {
        ItemMeta m = item.getItemMeta();
        m.setDisplayName(name);
        item.setItemMeta(m);
        return this;
    }

    public ItemBuilder lore(String... lines)
    {
        List<String> lore = Lists.newArrayList();

        Collections.addAll(lore, lines);

        ItemMeta m = item.getItemMeta();
        m.setLore(lore);
        item.setItemMeta(m);
        return this;
    }

    public ItemStack item()
    {
        ItemMeta meta = item.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        item.setItemMeta(meta);

        return item;
    }
}
