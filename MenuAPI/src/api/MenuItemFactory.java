package api;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by JPaul on 3/9/2016.
 */
public class MenuItemFactory
{
    public static MenuItem createItem(ItemStack item, OnExecute onExecute)
    {
        return BasicItem.create(item, onExecute);
    }

    public static MenuItem createItem(Material material, OnExecute onExecute)
    {
        return BasicItem.create(new ItemStack(material), onExecute);
    }

    public static MenuItem createItem(String name, Material material, OnExecute onExecute)
    {
        return BasicItem.create(name, new ItemStack(material), onExecute);
    }

    public static MenuItem createItem(ItemMeta meta, Material material, OnExecute onExecute)
    {
        ItemStack item = new ItemStack(material);
        item.setItemMeta(meta);

        return BasicItem.create(item, onExecute);
    }
}
