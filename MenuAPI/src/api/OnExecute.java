package api;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

/**
 * Created by JPaul on 3/9/2016.
 */
public interface OnExecute
{
    void execute(Player player, ClickType clickType);
}
