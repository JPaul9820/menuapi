package api;

/**
 * Created by JPaul on 3/11/2016.
 */
public class Manager
{
    static MenuManager MENU_MANAGER = new MenuManager();

    public static MenuManager getMenuManager()
    {
        if (MENU_MANAGER == null)
        {
            return new MenuManager();
        }

        return MENU_MANAGER;
    }
}
