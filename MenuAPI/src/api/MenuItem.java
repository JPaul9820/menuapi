package api;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

/**
 * Created by JPaul on 3/8/2016.
 */
public interface MenuItem
{
    ItemStack getItem();

    void onExecute(Player player, ClickType clickType);
}
