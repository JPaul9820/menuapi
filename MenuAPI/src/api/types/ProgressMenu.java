package api.types;

import com.google.common.collect.Lists;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Created by JPaul on 3/10/2016.
 */
public class ProgressMenu extends Menu
{
    List<Menu> menus;

    public ProgressMenu(int size, String name, List<Menu> menus)
    {
        super(size, name);

        List<Menu> modifiedMenus = Lists.newArrayList();

        for (Menu menu : menus)
        {
            menu.setParent(this);
            modifiedMenus.add(menu);
        }

        this.menus = modifiedMenus;
    }

    public void addMenu(Menu menu)
    {
        menus.add(menu);
    }

    @Override
    public void showToPlayer(Player... players)
    {
        for (Player player : players)
        {
            player.openInventory(this.menus.get(0).getInventory());
        }
    }
}
