package api.types;

import api.Container;
import api.Manager;
import api.MenuItem;
import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import java.util.Collection;
import java.util.Map;

/**
 * Created by JPaul on 3/8/2016.
 */
public class Menu implements Listener
{
    private Inventory inventory;
    private Map<Integer, MenuItem> menuItems;
    private int size;
    private String name;
    private Menu parent;

    public Menu(int size, String name)
    {
        Manager.getMenuManager().registerMenu(this);
        this.size = size;
        this.name = name;
        this.inventory = Bukkit.createInventory(null, size, name);
        this.menuItems = Maps.newHashMap();

        //had to make this class a listener to avoid unregistering each menu and to avoid concurrentmodification exception
        //register listener here
    }

    public int getSize()
    {
        return size;
    }

    public void setSize(int size)
    {
        this.size = size;
        this.inventory = Bukkit.createInventory(null, size, name);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Inventory getInventory()
    {
        return inventory;
    }

    public void setInventory(Inventory inventory)
    {
        this.inventory = inventory;
    }

    public Menu getParent()
    {
        return parent;
    }

    public void setParent(Menu parent)
    {
        this.parent = parent;
    }

    public MenuItem getMenuItemBySlot(int slot)
    {
        return menuItems.get(slot);
    }

    public Collection<MenuItem> getMenuItems()
    {
        return menuItems.values();
    }

    public void addItem(int slot, MenuItem menuItem)
    {
        addItem(slot, menuItem, null);
    }

    public <T> void addItem(int slot, MenuItem menuItem, @Nullable T data)
    {
        if (data != null)
        {
            if (menuItem instanceof Container)
            {
                ((Container<T>) menuItem).setData(data);
            }
        }

        inventory.setItem(slot, menuItem.getItem());
        menuItems.put(slot, menuItem);
    }

    public void showToPlayer(Player... players)
    {
        for (Player player : players)
        {
            player.openInventory(this.inventory);
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent clickEvent)
    {
        if (this.inventory.equals(clickEvent.getClickedInventory()))
        {
            if (this.menuItems.get(clickEvent.getSlot()) != null)
            {
                MenuItem menuItem = this.menuItems.get(clickEvent.getSlot());
                menuItem.onExecute((Player) clickEvent.getWhoClicked(), clickEvent.getClick());
            }

            clickEvent.setCancelled(true);
        }
    }
}
