package api;

import api.testingItemGen.ParentItemMenu;
import api.testingItemGen.armourMenu.ArmourMenu;
import api.testingItemGen.rarityMenu.RarityMenu;
import api.testingItemGen.selectorMenu.SelectorMenu;
import api.testingItemGen.tierChoice.TierMenu;
import api.testingItemGen.weaponChoice.WeaponMenu;
import api.types.Menu;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerEggThrowEvent;

import java.util.Arrays;
import java.util.List;

/**
 * Created by JPaul on 3/8/2016.
 */
public class MenuListener implements Listener
{
    @EventHandler
    public void test(PlayerEggThrowEvent throwEvent)
    {
        List<Menu> menus = Arrays.asList(new SelectorMenu(), new TierMenu(), new ArmourMenu(), new WeaponMenu(), new RarityMenu());

        ParentItemMenu itemMenu = new ParentItemMenu(9, "test", menus);

        itemMenu.showToPlayer(throwEvent.getPlayer());
    }
}
