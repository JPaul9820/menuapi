package api;

import api.types.Menu;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Map;

/**
 * Created by JPaul on 3/8/2016.
 */
public class MenuManager
{
    List<Menu> menus = Lists.newArrayList();
    Map<Class<? extends Menu>, Menu> classMenuMap = Maps.newHashMap();

    public void registerMenu(Menu menu)
    {
        menus.add(menu);
        classMenuMap.put(menu.getClass(), menu);
    }

    public void unregisterMenu(Menu menu)
    {
        menus.remove(menu);
        classMenuMap.remove(menu.getClass());
    }

    public List<Menu> getMenus()
    {
        return menus;
    }

    public Menu getMenu(Class<? extends Menu> menuClass)
    {
        if (classMenuMap.get(menuClass) == null)
        {
            //LOG.warning("MENU ERROR: " + menuClass + " is not registered");
            throw new NullPointerException();
        }
        else
        {
            return classMenuMap.get(menuClass);
        }
    }

    public Menu getMenu(Player player)
    {
        for (Menu menu : menus)
        {
            if (ChatColor.stripColor(menu.getInventory().getTitle()).equals(ChatColor.stripColor(player.getOpenInventory().getTitle())))
            {
                return menu;
            }
        }

        return null;
    }
}
